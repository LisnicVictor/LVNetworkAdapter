# LVNetworkAdapter

[![CI Status](http://img.shields.io/travis/Lisnic_Victor/LVNetworkAdapter.svg?style=flat)](https://travis-ci.org/Lisnic_Victor/LVNetworkAdapter)
[![Version](https://img.shields.io/cocoapods/v/LVNetworkAdapter.svg?style=flat)](http://cocoapods.org/pods/LVNetworkAdapter)
[![License](https://img.shields.io/cocoapods/l/LVNetworkAdapter.svg?style=flat)](http://cocoapods.org/pods/LVNetworkAdapter)
[![Platform](https://img.shields.io/cocoapods/p/LVNetworkAdapter.svg?style=flat)](http://cocoapods.org/pods/LVNetworkAdapter)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVNetworkAdapter is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LVNetworkAdapter"
```

## Author

Lisnic_Victor, gerasim.kisslin@gmail.com

## License

LVNetworkAdapter is available under the MIT license. See the LICENSE file for more info.
