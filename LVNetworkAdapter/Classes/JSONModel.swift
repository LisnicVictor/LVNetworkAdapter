//
//  JSONModel.swift
//  LVNetworkAdapter
//
//  Created by Victor Lisnic on 12/27/17.
//

import Foundation

public enum JSONParseError : Error {
    case failedToParse(message:String)
}

public protocol JSONModel : Codable {
    init(data: Data?, keyPath: String?) throws
}

public extension JSONModel {
    public init(data: Data?, keyPath: String? = nil) throws {

        guard let data = data else { throw JSONParseError.failedToParse(message: "data is nil") }

        if let keyPath = keyPath {
            let topLevel = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (topLevel as AnyObject).value(forKeyPath: keyPath) else { throw JSONParseError.failedToParse(message:" not found object for keypath : \(keyPath) ") }
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try JSONDecoder().decode(Self.self, from: nestedData)
            return
        }
        self = try JSONDecoder().decode(Self.self, from: data)
    }
}

public extension Array where Element : JSONModel {
    public init(data: Data?, keyPath: String? = nil) throws {
        guard let data = data else { throw JSONParseError.failedToParse(message:"data is nil" ) }

        if let keyPath = keyPath {
            let topLevel = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (topLevel as AnyObject).value(forKeyPath: keyPath) else { throw JSONParseError.failedToParse(message: " not found object for keypath : \(keyPath) ") }
            let nestedData = try JSONSerialization.data(withJSONObject: nestedJson)
            self = try JSONDecoder().decode([Element].self, from: nestedData)
            return
        }
        self = try JSONDecoder().decode([Element].self, from: data)
    }
}

