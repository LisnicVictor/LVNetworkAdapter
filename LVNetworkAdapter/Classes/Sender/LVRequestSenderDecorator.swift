//
//  LVRequestSenderDecorator.swift
//  Pods
//
//  Created by Victor Lisnic on 8/9/17.
//
//

import Foundation
import LVResult

public protocol LVRequestSenderDecorator : LVRequestSending
{
    var target : LVRequestSending {get set}
}

public extension LVRequestSenderDecorator {

    public var errorInterpreter: LVErrorInterpreting? {
        get { return target.errorInterpreter }
        set { target.errorInterpreter = newValue }
    }

    public func send(request: LVRequestType, completion: ((Result<Data?>) -> ())?) {
        target.send(request: request, completion: completion)
    }
}
