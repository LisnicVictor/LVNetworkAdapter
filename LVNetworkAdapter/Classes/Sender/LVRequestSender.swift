//
//  LVSender.swift
//  Pods
//
//  Created by Victor Lisnic on 8/9/17.
//
//

import Foundation
import LVResult

public protocol LVRequestSending {
    var errorInterpreter : LVErrorInterpreting? {get set}
    func send(request:LVRequestType,completion: ( (Result<Data?>) -> () )?)
}

public extension LVRequestSending {
    public var errorInterpreter : LVErrorInterpreting? {
        return nil
    }
}

public struct LVRequestSender : LVRequestSending {
    public var errorInterpreter : LVErrorInterpreting?

    public init(with errorInterpreter: LVErrorInterpreting? = nil) {
        self.errorInterpreter = errorInterpreter
    }

    public func send(request: LVRequestType, completion: ( (Result<Data?>) -> ())?) {
        let urlRequest = request.request
        URLSession(configuration:URLSessionConfiguration.default).dataTask(with: urlRequest)
        { (data, response, error) in
            if let error = error {
                completion?(Result.error(error))
            }
            if let networkError = self.errorInterpreter?.error(with: response, data: data) {
                completion?(Result.error(networkError))
            }
            else {
                completion?(Result.success(data))
            }
            }.resume()
    }
}
