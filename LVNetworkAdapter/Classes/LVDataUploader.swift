//
//  LVDataUploader.swift
//  Pods
//
//  Created by Victor Lisnic on 8/9/17.
//
//

import Foundation
import LVResult

public enum UploadProgress {
    case pending
    case uploading(progress:Double)
    case processingData
    case done
    case error
}

public typealias DataUploadingProgresslock = (UploadProgress) -> Void

public protocol DataUploading : LVRequestSending {
    func uploadData(with request:LVRequestType, progressBlock: (DataUploadingProgresslock)? , completion: ((Result<Data?>) -> ())? )
}

public extension DataUploading {
    public func send(request: LVRequestType, completion: ((Result<Data?>) -> ())?) {
        uploadData(with: request, progressBlock: nil, completion: completion)
    }
}

class TaskDelegate : NSObject, URLSessionDataDelegate {
    var progressClosure : DataUploadingProgresslock?

    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let progressPercentage = Double(totalBytesSent) / Double(totalBytesExpectedToSend)
        let progress = UploadProgress.uploading(progress: progressPercentage)
        print(progressPercentage)
        progressClosure?(progress)
    }
}

public struct DataUploader : DataUploading {
    public var errorInterpreter : LVErrorInterpreting?
    private var sessionDelegate = TaskDelegate()

    public init(with errorInterpreter: LVErrorInterpreting? = nil) {
        self.errorInterpreter = errorInterpreter
    }

    public func uploadData(with request:LVRequestType, progressBlock: (DataUploadingProgresslock)? , completion: ((Result<Data?>) -> ())? ) {

        sessionDelegate.progressClosure = progressBlock
        let urlRequest = request.request

        let delegateQueue = OperationQueue()
        delegateQueue.qualityOfService = .userInteractive

        let session = URLSession(configuration: .default, delegate: sessionDelegate, delegateQueue: delegateQueue)
        let task = session.uploadTask(with: urlRequest, from: urlRequest.httpBody) { (data, response, error) in
            if let error = error {
                completion?(Result.error(error))
            }
            if let networkError = self.errorInterpreter?.error(with: response, data: data) {
                completion?(Result.error(networkError))
            }
            else {
                completion?(Result.success(data))
            }
        }
        task.resume()
    }
}


