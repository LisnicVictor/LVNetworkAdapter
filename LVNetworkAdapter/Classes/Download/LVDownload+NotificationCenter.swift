//
//  LVDownload+NotificationCenter.swift
//  LVNetworkAdapter
//
//  Created by Victor Lisnic on 3/1/18.
//

import Foundation

public extension NotificationCenter {
    public func subscribeForLVManagerProgressChange(with block:@escaping (Download)->()) {
        addObserver(forName: .LVDownloadManagerProgress , object: nil, queue: .main) { notification in
            guard let download = notification.userInfo?["download"] as? Download else {return}
            block(download)
        }
    }

    public func subscribeForLVManagerStateChange(with block:@escaping (Download)->()) {
        addObserver(forName: .LVDownloadManagerState , object: nil, queue: .main) { notification in
            guard let download = notification.userInfo?["download"] as? Download else {return}
            block(download)
        }
    }
}
