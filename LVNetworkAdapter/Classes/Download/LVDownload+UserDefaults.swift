//
//  LVDownload+UserDefaults.swift
//  LVNetworkAdapter
//
//  Created by Victor Lisnic on 3/1/18.
//

import Foundation

fileprivate enum DownloadUDKeys: String {
    case lvDownloads
}

protocol DownloadPersistantManaging {
    func sync(downloads:[Download])
    func getDownloads() -> [Download]
}

struct DownloadPersistantManager: DownloadPersistantManaging {
    func getDownloads() -> [Download] {
        guard let data = UserDefaults.standard.data(forKey: DownloadUDKeys.lvDownloads.rawValue),
            let downloads = try? PropertyListDecoder().decode([Download].self, from: data) else {return []}
        return downloads
    }

    func sync(downloads: [Download]) {
        let data = try? PropertyListEncoder().encode(downloads)
        UserDefaults.standard.set(data, forKey: DownloadUDKeys.lvDownloads.rawValue)
    }
}
