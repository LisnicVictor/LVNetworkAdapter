//
//  LVDownloadManager.swift
//  LVNetworkAdapter
//
//  Created by Victor Lisnic on 2/28/18.
//

import Foundation
import LVResult

public extension Notification.Name {
    static var LVDownloadManagerProgress: Notification.Name  { return Notification.Name("LVDownloadManagetProgress") }
    static var LVDownloadManagerState: Notification.Name  { return Notification.Name("LVDownloadManagetState") }
}

public protocol LVDownloadManagerDelegate: class {
    func lvDownloadManagerFinished(download:Download)
    func lvDownloadManagerCompleted(download:Result<Download>)
    func lvDownloadManagerUpdatedProgress(for download:Download)
    func lvDownloadManagerPaused(download:Download)
    func lvDownloadManagerResumed(download:Download)
    func lvDownloadManagerCanceled(download:Download)
}

public extension LVDownloadManagerDelegate {
    func lvDownloadManagerCompleted(download:Result<Download>) {}
    func lvDownloadManagerUpdatedProgress(for download:Download) {}
    func lvDownloadManagerPaused(download:Download) {}
    func lvDownloadManagerResumed(download:Download) {}
    func lvDownloadManagerCanceled(download:Download) {}
}

public protocol LVDownloadManaging {
    func download(fileAt url:URL, id:String, userInfo:[String:String]?)
    func pauseDownload(with id:String, completion: @escaping ()->())
    func resumeDownload(with id:String)
    func cancelDownload(with id:String, completion: @escaping ()->())
    func deleteDownload(with id:String)

    func didAwake(with sessionIdentifier:String, completion:@escaping()->())

    var downloads: [Download] {get}

    weak var delegate: LVDownloadManagerDelegate? {get set}
}

public class LVDownloadManager: NSObject {

    public static let shared = LVDownloadManager()

    fileprivate static var identifier = "LVDownloadManager"

    var backgroundCompletionHandler: (()->())?
    public var downloads = [Download]()
    public weak var delegate: LVDownloadManagerDelegate?
    fileprivate lazy var session: URLSession = {
        let sessionConfigurator = URLSessionConfiguration.background(withIdentifier: LVDownloadManager.identifier)
        return URLSession(configuration: sessionConfigurator, delegate: self, delegateQueue: nil)
    }()

    public override init() {
        super.init()
        downloads = DownloadPersistantManager().getDownloads()

        NotificationCenter.default.addObserver(forName: Notification.Name.UIApplicationWillTerminate, object: nil, queue: .main) {[downloads = self.downloads] _ in
            DownloadPersistantManager().sync(downloads: downloads)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        DownloadPersistantManager().sync(downloads: downloads)
    }
}

//MARK: Interface implementation
extension LVDownloadManager: LVDownloadManaging {

    public func didAwake(with sessionIdentifier: String, completion: @escaping () -> ()) {
        if sessionIdentifier == LVDownloadManager.identifier {
            backgroundCompletionHandler = completion
            _ = session.sessionDescription
        }
    }

    public func download(fileAt url: URL, id: String, userInfo: [String : String]?) {
        let task = session.downloadTask(with: url)
        task.resume()

        var download = Download(url: url, id: id, userInfo: userInfo)
        download.taskIdentifier = task.taskIdentifier

        downloads.append(download)
    }

    public func pauseDownload(with id: String, completion: @escaping ()->()) {
        guard let index = downloads.index(where: { $0.id == id }) else {return}
        let taskIdentifier = downloads[index].taskIdentifier

        session.getTasksWithCompletionHandler { [unowned self] _, _, tasks in
            tasks.first(where: { $0.taskIdentifier == taskIdentifier })?.cancel {[unowned self] data in
                self.downloads[index].state = .paused
                self.downloads[index].resumeData = data
                self.postStateUpdate(for: self.downloads[index])
                self.delegate?.lvDownloadManagerPaused(download: self.downloads[index])
                completion()
            }
        }
    }

    public func resumeDownload(with id: String) {
        guard let index = downloads.index(where: { $0.id == id }),
        let data = downloads[index].resumeData else {return}

        switch downloads[index].state {
        case .paused, .failed(reason: _):
            let task = session.downloadTask(withResumeData: data)

            downloads[index].taskIdentifier = task.taskIdentifier
            downloads[index].state = .pending

            task.resume()

            //Notify changes
            postStateUpdate(for: downloads[index])
            self.delegate?.lvDownloadManagerResumed(download: self.downloads[index])
        default:
            return
        }
    }

    public func cancelDownload(with id: String, completion: @escaping ()->()) {
        guard let index = downloads.index(where: { $0.id == id }) else {return}
        let taskIdentifier = downloads[index].taskIdentifier

        session.getTasksWithCompletionHandler { [unowned self] _, _, tasks in
            tasks.first(where: { $0.taskIdentifier == taskIdentifier })?.cancel()
            self.downloads[index].state = .canceled
            self.downloads[index].progress = .zero
            self.postStateUpdate(for: self.downloads[index])
            self.delegate?.lvDownloadManagerCanceled(download: self.downloads[index])
            self.deleteDownload(with: self.downloads[index].id)
            completion()
        }
    }

    public func deleteDownload(with id: String) {
        guard let index = downloads.index(where: { $0.id == id }) else {return}
        downloads.remove(at: index)
    }
}

//MARK: SessionDelegate
extension LVDownloadManager: URLSessionDownloadDelegate {

    public func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        session.finishTasksAndInvalidate()
        DispatchQueue.main.async {
            self.backgroundCompletionHandler?()
        }
    }

    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        print(session)
        print(error)
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let index = downloads.index(where: { $0.taskIdentifier == task.taskIdentifier }) else {return}

        if let error = error as? NSError,
            let resumeData = error.userInfo[NSURLSessionDownloadTaskResumeData] as? Data{
            downloads[index].resumeData = resumeData
        }

        downloads[index].state = error != nil ? .failed(reason:error!.localizedDescription) : .completed

        //Notify about changes
        postStateUpdate(for: downloads[index])
        let result: Result = error != nil ? .error(error!) : .success(downloads[index])
        delegate?.lvDownloadManagerCompleted(download: result)
    }

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let index = downloads.index(where: { $0.taskIdentifier == downloadTask.taskIdentifier }) else {return}
        downloads[index].temporaryPath = location
        delegate?.lvDownloadManagerFinished(download: downloads[index])
    }

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        guard let index = downloads.index(where: { $0.taskIdentifier == downloadTask.taskIdentifier }) else {return}
        downloads[index].progress = DownloadProgress(recieved: fileOffset, expected: expectedTotalBytes)
        downloads[index].state = .downloading

        //Notify about changes
        postProgressUpdate(for: downloads[index])
        postStateUpdate(for: downloads[index])
        delegate?.lvDownloadManagerResumed(download: downloads[index])
    }

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        guard let index = downloads.index(where: { $0.taskIdentifier == downloadTask.taskIdentifier }) else {return}
        downloads[index].progress = DownloadProgress(recieved: totalBytesWritten, expected: totalBytesExpectedToWrite)

        //Notify about changes
        postProgressUpdate(for: downloads[index])
        delegate?.lvDownloadManagerUpdatedProgress(for: downloads[index])
    }
}

//MARK: Notifications
fileprivate extension LVDownloadManager {
    func postProgressUpdate(for download:Download) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .LVDownloadManagerProgress, object: nil, userInfo: ["download":download])
        }
    }

    func postStateUpdate(for download:Download) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .LVDownloadManagerState, object: nil, userInfo: ["download":download])
        }
    }
}
