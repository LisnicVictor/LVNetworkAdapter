//
//  LVErrorInterpreting.swift
//  Pods
//
//  Created by Victor Lisnic on 8/9/17.
//
//

import Foundation

public protocol LVErrorInterpreting {
    func error(with response:URLResponse?, data:Data?) -> Error?
}
