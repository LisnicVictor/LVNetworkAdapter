//
//  LVRequest.swift
//  Pods
//
//  Created by Victor Lisnic on 8/9/17.
//
//

import Foundation

public enum HttpMethod : String {
    case POST
    case PUT
    case GET
    case PATCH
    case DELETE
}

public protocol LVRequestType {
    var url : URL {get}
    var method : HttpMethod {get}
    var request : URLRequest {get}
}

public struct LVRequest : LVRequestType {
    public var method: HttpMethod
    public var url: URL

    public init(with method:HttpMethod, url:URL) {
        self.method = method
        self.url = url
    }

    public var request: URLRequest {
        var request = URLRequest.init(url:url)
        request.httpMethod = method.rawValue
        return request
    }
}
