//
//  LVRequestDecorator.swift
//  Pods
//
//  Created by Victor Lisnic on 8/9/17.
//
//

import Foundation

public protocol LVRequestDecorator : LVRequestType {
    var target : LVRequestType {get}
}

public extension LVRequestDecorator {
    public var method: HttpMethod {
        get { return target.method }
    }

    public var request: URLRequest {
        get { return target.request }
    }

    public var url: URL {
        get { return target.url }
    }
}

