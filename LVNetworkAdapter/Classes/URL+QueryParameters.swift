//
//  URL+QueryParameters.swift
//  LVNetworkAdapter
//
//  Created by Victor Lisnic on 12/27/17.
//

import Foundation

public extension URL {
    public func appendingQuery(withParameters parameters:[URLQueryItem]) -> URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        if components.queryItems != nil {
            components.queryItems?.append(contentsOf: parameters)
        } else {
            components.queryItems = parameters }
        return components.url!
    }
}
